package view;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.TexturePaint;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import model.Card;

public class CardView extends JLabel
{
    private static final long serialVersionUID = 1L;

    private static TexturePaint bgPattern;
    static
    {
	final int w = 16, h = w;
	final BufferedImage tmpImg = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
	final Graphics gfx = tmpImg.getGraphics();
	gfx.setColor(Color.GRAY);
	gfx.fillRect(0, 0, w, h);
	gfx.setColor(Color.DARK_GRAY);
	gfx.fillRect(0, 0, w/2, h/2);
	gfx.fillRect(w/2, h/2, w/2, h/2);
	bgPattern = new TexturePaint(tmpImg, new Rectangle(w, h));
    }

    void setCard(Card card, float zoom)
    {
	if(card != null)
	{
	    final int width = card.layout.bgImage.getWidth(null), height = card.layout.bgImage.getHeight(null);

	    BufferedImage cardPane = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);

	    Graphics2D gfx = (Graphics2D) cardPane.getGraphics();    
	    gfx.addRenderingHints(new RenderingHints(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY));
	    gfx.addRenderingHints(new RenderingHints(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY));
	    gfx.addRenderingHints(new RenderingHints(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON));

	    gfx.drawImage(card.layout.bgImage, 0, 0, null);
	    gfx.drawImage(card.picture, card.layout.picturePosition.x, card.layout.picturePosition.y, null);
	    gfx.setFont(card.layout.titleFont); 
	    gfx.setColor(card.layout.titleFontColor);
	    gfx.drawString(card.title, card.layout.titlePosition.x, card.layout.titlePosition.y);
	    gfx.setFont(card.layout.idFont); 
	    gfx.setColor(card.layout.idFontColor);
	    gfx.drawString(card.id, card.layout.idPosition.x, card.layout.idPosition.y);
	    gfx.setFont(card.layout.descriptionFont);
	    gfx.setColor(card.layout.descriptionFontColor);
	    gfx.drawString(card.description, card.layout.descriptionPosition.x, card.layout.descriptionPosition.y);
	    gfx.drawImage(card.layout.frameImage, card.layout.picturePosition.x, card.layout.picturePosition.y, null);

	    final int iconWidth = (int) (width*zoom*0.01), iconHeight = (int) (height*zoom*0.01);
	    BufferedImage cardIcon = new BufferedImage(iconWidth, iconHeight, BufferedImage.TYPE_INT_ARGB);
	    gfx = (Graphics2D) cardIcon.getGraphics();
	    gfx.setComposite(AlphaComposite.Src);
	    gfx.addRenderingHints(new RenderingHints(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY));
	    gfx.addRenderingHints(new RenderingHints(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC));
	    gfx.addRenderingHints(new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON));
	    gfx.drawImage(cardPane, 0, 0, iconWidth, iconHeight, null);

	    setIcon(new ImageIcon(cardIcon));
	}
	else
	    setIcon(null);
    }

    @Override
    public void paintComponent(Graphics g)
    {
	Graphics2D gfx = (Graphics2D) g;
	Paint defaultPaint = gfx.getPaint();
	gfx.setPaint(bgPattern);
	gfx.fillRect(0, 0, getWidth(), getHeight());
	gfx.setPaint(defaultPaint);
	super.paintComponent(g);
    }
}
