package view;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileNameExtensionFilter;

import control.Main;
import control.Utils;
import model.Card;
import model.CardLayout;

public class CardEditorDialog extends JDialog
{
    private static final long serialVersionUID = 1L;
    private Card card, dummyCard;
    private CardView cardviewLayout;
    private CardView cardview;
    private JTextField txtPictureFilename;

    void preview()
    {
	if(card.layout != null)
	    cardview.setCard(card, 75f);
    }

    public CardEditorDialog(JFrame parent, Card cardToEdit)
    {
	final JDialog self = this;
	setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
	setSize(new Dimension(720, 720));
	setTitle("Card editor");
	setModal(true);
	setLocationRelativeTo(parent);
	try { setIconImage(Utils.loadImage("edit.png")); } 
	catch (IOException e) { e.printStackTrace(); }

	card = new Card(cardToEdit);
	if(card.picture == null)
	    card.picture = MainWindow.dummyImage;

	dummyCard = new Card();
	dummyCard.id = "<id here>";
	dummyCard.title = "<title here>";
	dummyCard.description = "<description here>";
	dummyCard.picture = MainWindow.dummyImage;

	JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
	getContentPane().add(tabbedPane, BorderLayout.CENTER);

	JPanel panelTabLayout = new JPanel();
	tabbedPane.addTab("Layout", null, panelTabLayout, null);
	panelTabLayout.setLayout(new BoxLayout(panelTabLayout, BoxLayout.Y_AXIS));

	JPanel panelLayout = new JPanel();
	panelLayout.setPreferredSize(new Dimension(10, 36));
	panelLayout.setMinimumSize(new Dimension(10, 36));
	panelLayout.setMaximumSize(new Dimension(32767, 36));
	panelTabLayout.add(panelLayout);
	panelLayout.setLayout(new BoxLayout(panelLayout, BoxLayout.X_AXIS));

	JLabel lblLayout = new JLabel("  Layout:   ");
	panelLayout.add(lblLayout);

	CardLayout nullCardLayout = new CardLayout();
	nullCardLayout.id = "none";
	Vector<CardLayout> comboBoxModelList = new Vector<CardLayout>();
	comboBoxModelList.add(nullCardLayout);
	comboBoxModelList.addAll(Main.layouts);
	final JComboBox<CardLayout> comboBoxLayout = new JComboBox<CardLayout>(comboBoxModelList);
	if(card.layout != null)
	    comboBoxLayout.setSelectedItem(card.layout);
	comboBoxLayout.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent arg0) {
		if(comboBoxLayout.getSelectedIndex() == 0)
		{
		    cardviewLayout.setCard(null, 33f);

		    card.layout = null;
		    cardview.setCard(null, 75f);
		}
		else
		{
		    dummyCard.layout = (CardLayout) comboBoxLayout.getSelectedItem();
		    cardviewLayout.setCard(dummyCard, 33f);

		    card.layout = dummyCard.layout;
		    cardview.setCard(card, 75f);
		}
	    }
	});
	comboBoxLayout.setMaximumSize(new Dimension(32767, 24));
	panelLayout.add(comboBoxLayout);

	Component horizontalStrut = Box.createHorizontalStrut(20);
	panelLayout.add(horizontalStrut);

	cardviewLayout = new CardView();
	panelTabLayout.add(cardviewLayout);
	cardviewLayout.setHorizontalAlignment(SwingConstants.CENTER);

	JPanel panelTabValues = new JPanel();
	tabbedPane.addTab("Values", null, panelTabValues, null);
	panelTabValues.setLayout(new BorderLayout(0, 0));

	cardview = new CardView();
	cardview.setHorizontalAlignment(SwingConstants.CENTER);
	panelTabValues.add(cardview, BorderLayout.CENTER);

	JPanel panelFields = new JPanel();
	panelFields.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
	panelTabValues.add(panelFields, BorderLayout.EAST);
	panelFields.setLayout(new BoxLayout(panelFields, BoxLayout.Y_AXIS));

	JPanel panelTitle = new JPanel();
	panelTitle.setMaximumSize(new Dimension(32767, 36));
	panelFields.add(panelTitle);
	panelTitle.setLayout(new BoxLayout(panelTitle, BoxLayout.X_AXIS));

	final JCheckBox chckbxPreviewAutomatically = new JCheckBox("Preview automatically");

	JLabel lblTitle = new JLabel("Title:");
	lblTitle.setBorder(new EmptyBorder(0, 4, 0, 4));
	panelTitle.add(lblTitle);

	final JTextField txtTitle = new JTextField(card.title);
	txtTitle.getDocument().addDocumentListener(new DocumentListener()
	{
	    @Override public void removeUpdate(DocumentEvent arg0) { changedUpdate(arg0); }
	    @Override public void insertUpdate(DocumentEvent arg0) { changedUpdate(arg0); }
	    @Override public void changedUpdate(DocumentEvent arg0) {
		card.title = txtTitle.getText();
		if(chckbxPreviewAutomatically.isSelected())
		    preview();
	    }
	});
	txtTitle.setMaximumSize(new Dimension(2147483647, 24));
	panelTitle.add(txtTitle);

	JPanel panelID = new JPanel();
	panelID.setMaximumSize(new Dimension(32767, 36));
	panelFields.add(panelID);
	panelID.setLayout(new BoxLayout(panelID, BoxLayout.X_AXIS));

	JLabel lblId = new JLabel("ID:");
	lblId.setBorder(new EmptyBorder(0, 4, 0, 4));
	panelID.add(lblId);

	final JTextField txtId = new JTextField(card.id);
	txtId.getDocument().addDocumentListener(new DocumentListener()
	{
	    @Override public void removeUpdate(DocumentEvent arg0) { changedUpdate(arg0); }
	    @Override public void insertUpdate(DocumentEvent arg0) { changedUpdate(arg0); }
	    @Override public void changedUpdate(DocumentEvent arg0) {
		card.id = txtId.getText();
		if(chckbxPreviewAutomatically.isSelected())
		    preview();
	    }
	});
	txtId.setMaximumSize(new Dimension(2147483647, 24));
	panelID.add(txtId);

	JPanel panelDesc = new JPanel();
	panelDesc.setBorder(new EmptyBorder(8, 0, 8, 0));
	panelDesc.setMaximumSize(new Dimension(32767, 128));
	panelFields.add(panelDesc);
	panelDesc.setLayout(new BoxLayout(panelDesc, BoxLayout.Y_AXIS));

	JLabel lblDescription = new JLabel("Description:");
	panelDesc.add(lblDescription);

	final JTextArea txtrDesc = new JTextArea(card.description);
	txtrDesc.setLineWrap(true);
	txtrDesc.setMaximumSize(new Dimension(400, 150));
	txtrDesc.getDocument().addDocumentListener(new DocumentListener()
	{
	    @Override public void removeUpdate(DocumentEvent arg0) { changedUpdate(arg0); }
	    @Override public void insertUpdate(DocumentEvent arg0) { changedUpdate(arg0); }
	    @Override public void changedUpdate(DocumentEvent arg0) {
		card.description = txtrDesc.getText();
		if(chckbxPreviewAutomatically.isSelected())
		    preview();
	    }
	});
	panelDesc.add(txtrDesc);

	JPanel panelPictureFilename = new JPanel();
	panelPictureFilename.setMaximumSize(new Dimension(32767, 36));
	panelFields.add(panelPictureFilename);
	panelPictureFilename.setLayout(new BoxLayout(panelPictureFilename, BoxLayout.X_AXIS));

	JLabel lblPicture = new JLabel("Picture:");
	lblPicture.setBorder(new EmptyBorder(0, 4, 0, 4));
	panelPictureFilename.add(lblPicture);

	txtPictureFilename = new JTextField(card.properties.getProperty("picture"));
	txtPictureFilename.setMaximumSize(new Dimension(2147483647, 24));
	txtPictureFilename.setEditable(false);
	panelPictureFilename.add(txtPictureFilename);
	txtPictureFilename.setColumns(10);

	JButton btnPickFile = new JButton("...");
	btnPickFile.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent arg0) {
		JFileChooser chooser = new JFileChooser(new File(Card.CARDS_FOLDER));
		chooser.setFileFilter(new FileNameExtensionFilter("Image files", ImageIO.getReaderFileSuffixes()));
		final int action = chooser.showOpenDialog(self);
		if(action == JFileChooser.APPROVE_OPTION)
		{
		    File file = chooser.getSelectedFile();
		    txtPictureFilename.setText(file.getName());
		    card.properties.put("picture", file.getName());
		    try { card.picture = Utils.loadImage(Card.CARDS_FOLDER+file.getName()); } 
		    catch (IOException e) { JOptionPane.showMessageDialog(self, e.getLocalizedMessage(), "Error while loading picture", JOptionPane.ERROR_MESSAGE); }
		    if(chckbxPreviewAutomatically.isSelected())
			preview();
		}
	    }
	});
	panelPictureFilename.add(btnPickFile);

	JButton btnPreview = new JButton("Preview");
	btnPreview.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent arg0) {
		preview();
	    }
	});

	chckbxPreviewAutomatically.setSelected(true);
	panelFields.add(chckbxPreviewAutomatically);
	panelFields.add(btnPreview);

	JPanel panelButtons = new JPanel();
	getContentPane().add(panelButtons, BorderLayout.SOUTH);

	JButton btnSave = new JButton("Save");
	btnSave.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent arg0) {
		if(card.layout == null)
		    JOptionPane.showMessageDialog(self, "Can't save card without layout", "No model selected", JOptionPane.WARNING_MESSAGE);
		else if(card.picture == null || txtPictureFilename.getText().isEmpty())
		    JOptionPane.showMessageDialog(self, "Card picture was not selected", "No card picture", JOptionPane.WARNING_MESSAGE);
		else
		{
		    JFileChooser chooser = new JFileChooser(new File(Card.CARDS_FOLDER));
		    chooser.setSelectedFile(new File(Card.CARDS_FOLDER + card.id + ".properties"));
		    final int action = chooser.showSaveDialog(self);
		    if(action == JFileChooser.APPROVE_OPTION)
		    {
			try 
			{ 
			    card.save(chooser.getSelectedFile());
			    Card cardToReplace = null;
			    for(Card c : Main.cards)
				if(c.id == card.id)
				    cardToReplace = c;
			    if(cardToReplace != null) 
				Main.cards.remove(cardToReplace);
			    Main.cards.add(card);
			    dispose();
			} 
			catch (IOException e) 
			{ 
			    JOptionPane.showMessageDialog(self, e.getLocalizedMessage(), "Could not save card", JOptionPane.ERROR_MESSAGE); 
			}
		    }
		}
	    }
	});
	panelButtons.add(btnSave);

	JButton btnCancel = new JButton("Cancel");
	btnCancel.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent arg0) {
		self.dispose();
	    }
	});
	panelButtons.add(btnCancel);

	preview();
    }
}
