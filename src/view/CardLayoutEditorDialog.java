package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileNameExtensionFilter;

import control.Utils;
import model.Card;
import model.CardLayout;
import say.swing.JFontChooser;
import view.ColorChooserButton.ColorChangedListener;

public class CardLayoutEditorDialog extends JDialog 
{
    private static final long serialVersionUID = 1L;
    private JTextField txtId;
    private JTextField textFieldName;
    private JTextField txtBgfilename;
    private JTextField txtFrameFilename;
    private JTextField txtTitleFont;
    private JTextField txtCardIdFont;
    private JTextField txtCardDescFont;
    private JCheckBox chckbxPreviewAutomatically;
    private CardView cardview;

    CardLayout layout = new CardLayout();
    private JTextField txtTitleColor;
    private JTextField txtCardIdColor;
    private JTextField txtCardDescColor;

    void preview()
    {
	Card tmpCard = new Card();
	tmpCard.id = "<id here>";
	tmpCard.title = "<title here>";
	tmpCard.description = "<description here>";
	tmpCard.picture = MainWindow.dummyImage;
	tmpCard.layout = layout;
	cardview.setCard(tmpCard, 75f);
    }

    private static String toHumanString(Color c)
    {
	return c.getRed() + ", " + c.getGreen() + ", " + c.getBlue();
    }

    public CardLayoutEditorDialog(JFrame parent)
    {
	final JDialog self = this;
	setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
	setSize(new Dimension(720, 720));
	setTitle("Card layout editor");
	setModal(true);
	setLocationRelativeTo(parent);
	try { setIconImage(Utils.loadImage("edit.png")); } 
	catch (IOException e) { e.printStackTrace(); }

	JPanel panelButtons = new JPanel();
	getContentPane().add(panelButtons, BorderLayout.SOUTH);

	JButton btnSave = new JButton("Save");
	panelButtons.add(btnSave);

	JButton btnCancel = new JButton("Cancel");
	btnCancel.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent arg0) {
		dispose();
	    }
	});
	panelButtons.add(btnCancel);

	// init layout images with temp
	layout.bgImage = layout.frameImage = MainWindow.dummyImage;

	JPanel panel = new JPanel();
	panel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
	getContentPane().add(panel, BorderLayout.EAST);
	panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

	JPanel panelID = new JPanel();
	panelID.setMaximumSize(new Dimension(32767, 36));
	panel.add(panelID);
	panelID.setLayout(new BoxLayout(panelID, BoxLayout.X_AXIS));

	JLabel labelID = new JLabel("Layout ID:");
	labelID.setBorder(new EmptyBorder(0, 4, 0, 4));
	panelID.add(labelID);

	txtId = new JTextField();
	txtId.addKeyListener(new KeyAdapter() {
	    @Override
	    public void keyTyped(KeyEvent arg0) {
		layout.id = txtId.getText();
	    }
	});
	txtId.setMaximumSize(new Dimension(2147483647, 24));
	panelID.add(txtId);
	txtId.setColumns(10);

	JPanel panelName = new JPanel();
	panelName.setBorder(new EmptyBorder(0, 4, 0, 4));
	panelName.setMaximumSize(new Dimension(32767, 36));
	panel.add(panelName);
	panelName.setLayout(new BoxLayout(panelName, BoxLayout.X_AXIS));

	JLabel lblName = new JLabel("Layout name:");
	panelName.add(lblName);

	textFieldName = new JTextField();
	textFieldName.addKeyListener(new KeyAdapter() {
	    @Override
	    public void keyTyped(KeyEvent e) {
		layout.name = textFieldName.getText();
	    }
	});
	textFieldName.setMaximumSize(new Dimension(2147483647, 24));
	panelName.add(textFieldName);
	textFieldName.setColumns(10);

	JPanel panel_1 = new JPanel();
	panel_1.setMaximumSize(new Dimension(32767, 36));
	panel.add(panel_1);
	panel_1.setLayout(new BoxLayout(panel_1, BoxLayout.X_AXIS));

	JLabel lblBackground = new JLabel("Background:");
	lblBackground.setBorder(new EmptyBorder(0, 4, 0, 4));
	panel_1.add(lblBackground);

	txtBgfilename = new JTextField();
	txtBgfilename.setEditable(false);
	txtBgfilename.setMaximumSize(new Dimension(2147483647, 24));
	panel_1.add(txtBgfilename);
	txtBgfilename.setColumns(10);

	JButton btnBgfilename = new JButton("...");
	btnBgfilename.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent arg0) {
		JFileChooser chooser = new JFileChooser(new File(CardLayout.LAYOUTS_FOLDER));
		chooser.setFileFilter(new FileNameExtensionFilter("Image files", ImageIO.getReaderFileSuffixes()));
		final int action = chooser.showOpenDialog(self);
		if(action == JFileChooser.APPROVE_OPTION)
		{
		    File file = chooser.getSelectedFile();
		    txtBgfilename.setText(file.getName());
		    layout.properties.put("bg_image", file.getName());
		    try { layout.bgImage = Utils.loadImage(CardLayout.LAYOUTS_FOLDER+file.getName()); } 
		    catch (IOException e) { JOptionPane.showMessageDialog(self, e.getLocalizedMessage(), "Error while loading image", JOptionPane.ERROR_MESSAGE); }
		    if(chckbxPreviewAutomatically.isSelected())
			preview();
		}
	    }
	});
	panel_1.add(btnBgfilename);

	JPanel panel_2 = new JPanel();
	panel_2.setMaximumSize(new Dimension(32767, 36));
	panel.add(panel_2);
	panel_2.setLayout(new BoxLayout(panel_2, BoxLayout.X_AXIS));

	JLabel lblFrame = new JLabel("Frame:");
	lblFrame.setBorder(new EmptyBorder(0, 4, 0, 4));
	panel_2.add(lblFrame);

	txtFrameFilename = new JTextField();
	txtFrameFilename.setEditable(false);
	txtFrameFilename.setMaximumSize(new Dimension(2147483647, 24));
	panel_2.add(txtFrameFilename);
	txtFrameFilename.setColumns(10);

	JButton btnFrameFilename = new JButton("...");
	btnFrameFilename.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent e) {
		JFileChooser chooser = new JFileChooser(new File(CardLayout.LAYOUTS_FOLDER));
		chooser.setFileFilter(new FileNameExtensionFilter("Image files", ImageIO.getReaderFileSuffixes()));
		final int action = chooser.showOpenDialog(self);
		if(action == JFileChooser.APPROVE_OPTION)
		{
		    File file = chooser.getSelectedFile();
		    txtFrameFilename.setText(file.getName());
		    layout.properties.put("frame_image", file.getName());
		    try { layout.frameImage = Utils.loadImage(CardLayout.LAYOUTS_FOLDER+file.getName()); } 
		    catch (IOException e1) { JOptionPane.showMessageDialog(self, e1.getLocalizedMessage(), "Error while loading image", JOptionPane.ERROR_MESSAGE); }
		    if(chckbxPreviewAutomatically.isSelected())
			preview();
		}
	    }
	});
	panel_2.add(btnFrameFilename);

	JPanel panel_3 = new JPanel();
	panel_3.setMaximumSize(new Dimension(32767, 36));
	panel.add(panel_3);
	panel_3.setLayout(new BoxLayout(panel_3, BoxLayout.X_AXIS));

	JLabel lblCardPicturePosition = new JLabel("Card picture position:");
	lblCardPicturePosition.setBorder(new EmptyBorder(0, 4, 0, 4));
	panel_3.add(lblCardPicturePosition);

	final JSpinner spinnerCardPicPosX = new JSpinner();
	spinnerCardPicPosX.addChangeListener(new ChangeListener() {
	    public void stateChanged(ChangeEvent arg0) {
		layout.picturePosition.x = (Integer) spinnerCardPicPosX.getValue();
		if(chckbxPreviewAutomatically.isSelected())
		    preview();
	    }
	});
	spinnerCardPicPosX.setMaximumSize(new Dimension(32767, 24));
	spinnerCardPicPosX.setValue(layout.picturePosition.x);
	panel_3.add(spinnerCardPicPosX);

	final JSpinner spinnerCardPicPosY = new JSpinner();
	spinnerCardPicPosY.addChangeListener(new ChangeListener() {
	    public void stateChanged(ChangeEvent e) {
		layout.picturePosition.y = (Integer) spinnerCardPicPosY.getValue();
		if(chckbxPreviewAutomatically.isSelected())
		    preview();
	    }
	});
	spinnerCardPicPosY.setMaximumSize(new Dimension(32767, 24));
	spinnerCardPicPosY.setValue(layout.picturePosition.y);
	panel_3.add(spinnerCardPicPosY);

	JPanel panel_4 = new JPanel();
	panel_4.setMaximumSize(new Dimension(32767, 36));
	panel.add(panel_4);
	panel_4.setLayout(new BoxLayout(panel_4, BoxLayout.X_AXIS));

	JLabel lblCardTitlePosition = new JLabel("Card title position:");
	lblCardTitlePosition.setBorder(new EmptyBorder(0, 4, 0, 4));
	panel_4.add(lblCardTitlePosition);

	final JSpinner spinnerCardTitlePosX = new JSpinner();
	spinnerCardTitlePosX.addChangeListener(new ChangeListener() {
	    public void stateChanged(ChangeEvent e) {
		layout.titlePosition.x = (Integer) spinnerCardTitlePosX.getValue();
		if(chckbxPreviewAutomatically.isSelected())
		    preview();
	    }
	});
	spinnerCardTitlePosX.setMaximumSize(new Dimension(32767, 24));
	spinnerCardTitlePosX.setValue(layout.titlePosition.x);
	panel_4.add(spinnerCardTitlePosX);

	final JSpinner spinnerCardTitlePosY = new JSpinner();
	spinnerCardTitlePosY.addChangeListener(new ChangeListener() {
	    public void stateChanged(ChangeEvent e) {
		layout.titlePosition.y = (Integer) spinnerCardTitlePosY.getValue();
		if(chckbxPreviewAutomatically.isSelected())
		    preview();
	    }
	});
	spinnerCardTitlePosY.setMaximumSize(new Dimension(32767, 24));
	spinnerCardTitlePosY.setValue(layout.titlePosition.y);
	panel_4.add(spinnerCardTitlePosY);

	JPanel panel_7 = new JPanel();
	panel_7.setMaximumSize(new Dimension(32767, 36));
	panel.add(panel_7);
	panel_7.setLayout(new BoxLayout(panel_7, BoxLayout.X_AXIS));

	JLabel lblCardTitleFont = new JLabel("Card title font:");
	lblCardTitleFont.setBorder(new EmptyBorder(0, 4, 0, 4));
	panel_7.add(lblCardTitleFont);

	txtTitleFont = new JTextField();
	txtTitleFont.setEditable(false);
	txtTitleFont.setMaximumSize(new Dimension(2147483647, 24));
	panel_7.add(txtTitleFont);
	txtTitleFont.setColumns(10);

	JButton btnFontChooser = new JButton("...");
	btnFontChooser.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent e) {
		JFontChooser chooser = new JFontChooser();
		chooser.showDialog(self);
		if(chooser.getSelectedFont() != null)
		{
		    Font font = chooser.getSelectedFont();
		    txtTitleFont.setText(font.getName());
		    layout.properties.put("title_font", font.getName());
		    layout.properties.put("title_font_size", font.getSize());
		    layout.properties.put("title_font_style", (font.getStyle() == Font.BOLD? "bold" : font.getStyle() == Font.ITALIC? "italic" : "plain"));
		    layout.titleFont = font;
		    if(chckbxPreviewAutomatically.isSelected())
			preview();
		}
	    }
	});
	panel_7.add(btnFontChooser);

	JPanel panel_10 = new JPanel();
	panel_10.setMaximumSize(new Dimension(32767, 36));
	panel.add(panel_10);
	panel_10.setLayout(new BoxLayout(panel_10, BoxLayout.X_AXIS));

	JLabel lblCardTitleColor = new JLabel("Card title color:");
	lblCardTitleColor.setBorder(new EmptyBorder(0, 4, 0, 4));
	panel_10.add(lblCardTitleColor);

	txtTitleColor = new JTextField();
	txtTitleColor.setEditable(false);
	txtTitleColor.setMaximumSize(new Dimension(2147483647, 24));
	panel_10.add(txtTitleColor);
	txtTitleColor.setColumns(10);

	ColorChooserButton btnTitleColor = new ColorChooserButton(layout.titleFontColor);
	btnTitleColor.addColorChangedListener(new ColorChangedListener() {
	    public void colorChanged(Color newColor) {
		if(newColor != null)
		{
		    layout.titleFontColor = newColor;
		    txtTitleColor.setText(toHumanString(newColor));
		    if(chckbxPreviewAutomatically.isSelected())
			preview();
		}
	    }
	});
	panel_10.add(btnTitleColor);

	JPanel panel_5 = new JPanel();
	panel_5.setMaximumSize(new Dimension(32767, 36));
	panel.add(panel_5);
	panel_5.setLayout(new BoxLayout(panel_5, BoxLayout.X_AXIS));

	JLabel lblCardIdPosition = new JLabel("Card ID position:");
	lblCardIdPosition.setBorder(new EmptyBorder(0, 4, 0, 4));
	panel_5.add(lblCardIdPosition);

	final JSpinner spinnerCardIdPosX = new JSpinner();
	spinnerCardIdPosX.addChangeListener(new ChangeListener() {
	    public void stateChanged(ChangeEvent arg0) {
		layout.idPosition.x = (Integer) spinnerCardIdPosX.getValue();
		if(chckbxPreviewAutomatically.isSelected())
		    preview();
	    }
	});
	spinnerCardIdPosX.setMaximumSize(new Dimension(32767, 24));
	spinnerCardIdPosX.setValue(layout.idPosition.x);
	panel_5.add(spinnerCardIdPosX);

	final JSpinner spinnerCardIdPosY = new JSpinner();
	spinnerCardIdPosY.addChangeListener(new ChangeListener() {
	    public void stateChanged(ChangeEvent e) {
		layout.idPosition.y = (Integer) spinnerCardIdPosY.getValue();
		if(chckbxPreviewAutomatically.isSelected())
		    preview();
	    }
	});
	spinnerCardIdPosY.setMaximumSize(new Dimension(32767, 24));
	spinnerCardIdPosY.setValue(layout.idPosition.y);
	panel_5.add(spinnerCardIdPosY);

	JPanel panel_8 = new JPanel();
	panel_8.setMaximumSize(new Dimension(32767, 36));
	panel.add(panel_8);
	panel_8.setLayout(new BoxLayout(panel_8, BoxLayout.X_AXIS));

	JLabel lblCardIdFont = new JLabel("Card ID font:");
	lblCardIdFont.setBorder(new EmptyBorder(0, 4, 0, 4));
	panel_8.add(lblCardIdFont);

	txtCardIdFont = new JTextField();
	txtCardIdFont.setEditable(false);
	txtCardIdFont.setMaximumSize(new Dimension(2147483647, 24));
	panel_8.add(txtCardIdFont);
	txtCardIdFont.setColumns(10);

	JButton btnCardIdFont = new JButton("...");
	btnCardIdFont.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent arg0) {
		JFontChooser chooser = new JFontChooser();
		chooser.showDialog(self);
		if(chooser.getSelectedFont() != null)
		{
		    Font font = chooser.getSelectedFont();
		    txtCardIdFont.setText(font.getName());
		    layout.properties.put("id_font", font.getName());
		    layout.properties.put("id_font_size", font.getSize());
		    layout.properties.put("id_font_style", (font.getStyle() == Font.BOLD? "bold" : font.getStyle() == Font.ITALIC? "italic" : "plain"));
		    layout.idFont = font;
		    if(chckbxPreviewAutomatically.isSelected())
			preview();
		}
	    }
	});
	panel_8.add(btnCardIdFont);

	JPanel panel_11 = new JPanel();
	panel_11.setMaximumSize(new Dimension(32767, 36));
	panel.add(panel_11);
	panel_11.setLayout(new BoxLayout(panel_11, BoxLayout.X_AXIS));

	JLabel lblCardIdColor = new JLabel("Card ID color:");
	lblCardIdColor.setBorder(new EmptyBorder(0, 4, 0, 4));
	panel_11.add(lblCardIdColor);

	txtCardIdColor = new JTextField();
	txtCardIdColor.setEditable(false);
	txtCardIdColor.setMaximumSize(new Dimension(2147483647, 24));
	panel_11.add(txtCardIdColor);
	txtCardIdColor.setColumns(10);

	ColorChooserButton btnCardIdColor = new ColorChooserButton(layout.idFontColor);
	btnCardIdColor.addColorChangedListener(new ColorChangedListener() {
	    public void colorChanged(Color newColor) {
		if(newColor != null)
		{
		    layout.idFontColor = newColor;
		    txtCardIdColor.setText(toHumanString(newColor));
		    if(chckbxPreviewAutomatically.isSelected())
			preview();
		}
	    }
	});
	panel_11.add(btnCardIdColor);

	JPanel panel_6 = new JPanel();
	panel_6.setMaximumSize(new Dimension(32767, 36));
	panel.add(panel_6);
	panel_6.setLayout(new BoxLayout(panel_6, BoxLayout.X_AXIS));

	JLabel lblCardDescPosition = new JLabel("Card desc. position:");
	lblCardDescPosition.setBorder(new EmptyBorder(0, 4, 0, 4));
	panel_6.add(lblCardDescPosition);

	final JSpinner spinnerCardDescPosX = new JSpinner();
	spinnerCardDescPosX.addChangeListener(new ChangeListener() {
	    public void stateChanged(ChangeEvent e) {
		layout.descriptionPosition.x = (Integer) spinnerCardDescPosX.getValue();
		if(chckbxPreviewAutomatically.isSelected())
		    preview();
	    }
	});
	spinnerCardDescPosX.setMaximumSize(new Dimension(32767, 24));
	spinnerCardDescPosX.setValue(layout.descriptionPosition.x);
	panel_6.add(spinnerCardDescPosX);

	final JSpinner spinnerCardDescPosY = new JSpinner();
	spinnerCardDescPosY.addChangeListener(new ChangeListener() {
	    public void stateChanged(ChangeEvent e) {
		layout.descriptionPosition.y = (Integer) spinnerCardDescPosY.getValue();
		if(chckbxPreviewAutomatically.isSelected())
		    preview();
	    }
	});
	spinnerCardDescPosY.setMaximumSize(new Dimension(32767, 24));
	spinnerCardDescPosY.setValue(layout.descriptionPosition.y);
	panel_6.add(spinnerCardDescPosY);

	JPanel panel_9 = new JPanel();
	panel_9.setMaximumSize(new Dimension(32767, 36));
	panel.add(panel_9);
	panel_9.setLayout(new BoxLayout(panel_9, BoxLayout.X_AXIS));

	JLabel lblCardDescFont = new JLabel("Card desc. font:");
	lblCardDescFont.setBorder(new EmptyBorder(0, 4, 0, 4));
	panel_9.add(lblCardDescFont);

	txtCardDescFont = new JTextField();
	txtCardDescFont.setEditable(false);
	txtCardDescFont.setMaximumSize(new Dimension(2147483647, 24));
	panel_9.add(txtCardDescFont);
	txtCardDescFont.setColumns(10);

	JButton btnCardDescFont = new JButton("...");
	btnCardDescFont.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent e) {
		JFontChooser chooser = new JFontChooser();
		chooser.showDialog(self);
		if(chooser.getSelectedFont() != null)
		{
		    Font font = chooser.getSelectedFont();
		    txtCardDescFont.setText(font.getName());
		    layout.properties.put("description_font", font.getName());
		    layout.properties.put("description_font_size", font.getSize());
		    layout.properties.put("description_font_style", (font.getStyle() == Font.BOLD? "bold" : font.getStyle() == Font.ITALIC? "italic" : "plain"));
		    layout.descriptionFont = font;
		    if(chckbxPreviewAutomatically.isSelected())
			preview();
		}
	    }
	});
	panel_9.add(btnCardDescFont);

	JButton btnPreview = new JButton("Preview");
	btnPreview.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent e) {
		preview();
	    }
	});

	JPanel panel_12 = new JPanel();
	panel_12.setMaximumSize(new Dimension(32767, 36));
	panel.add(panel_12);
	panel_12.setLayout(new BoxLayout(panel_12, BoxLayout.X_AXIS));

	JLabel lblCardDescColor = new JLabel("Card desc. color:");
	lblCardDescColor.setBorder(new EmptyBorder(0, 4, 0, 4));
	panel_12.add(lblCardDescColor);

	txtCardDescColor = new JTextField();
	txtCardDescColor.setEditable(false);
	txtCardDescColor.setMaximumSize(new Dimension(2147483647, 24));
	panel_12.add(txtCardDescColor);
	txtCardDescColor.setColumns(10);

	ColorChooserButton btnCardDescColor = new ColorChooserButton(layout.descriptionFontColor);
	btnCardDescColor.addColorChangedListener(new ColorChangedListener() {
	    public void colorChanged(Color newColor) {
		if(newColor != null)
		{
		    layout.descriptionFontColor = newColor;
		    txtCardDescColor.setText(toHumanString(newColor));
		    if(chckbxPreviewAutomatically.isSelected())
			preview();
		}
	    }
	});
	panel_12.add(btnCardDescColor);
	panel.add(btnPreview);

	chckbxPreviewAutomatically = new JCheckBox("Preview automatically");
	panel.add(chckbxPreviewAutomatically);

	cardview = new CardView();
	getContentPane().add(cardview, BorderLayout.CENTER);

    }
}
