package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.swing.Box;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JSpinner;
import javax.swing.JSplitPane;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import control.Main;
import control.Utils;
import model.Card;
import model.CardLayout;

public class MainWindow extends JFrame 
{
    private static final long serialVersionUID = 1L;
    private CardView cardview;
    private JSpinner spinner;
    private JList<Card> listCard;
    private JList<CardLayout> listLayout;
    Image icon = null;

    public static final BufferedImage dummyImage = new BufferedImage(400, 300, BufferedImage.TYPE_INT_ARGB);

    void openCardEditorDialog(Card cardToEdit)
    {
	new CardEditorDialog(this, cardToEdit).setVisible(true);
	Main.cards.sort(Card.Comparator);
	((DefaultListModel<Card>) listCard.getModel()).clear();
	for(Card card : Main.cards)
	    ((DefaultListModel<Card>) listCard.getModel()).addElement(card);
    }

    public MainWindow()
    {
	final JFrame self = this;
	setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
	setTitle("tcgtool");
	setSize(new Dimension(800, 920));
	setLocationRelativeTo(null);

	try { icon = Utils.loadImage("icon.png"); } 
	catch (IOException e) { e.printStackTrace(); }
	setIconImage(icon);

	JMenuBar menuBar = new JMenuBar();
	setJMenuBar(menuBar);

	JMenu mnFile = new JMenu("File");
	menuBar.add(mnFile);

	JMenu mnNew = new JMenu("New");
	mnFile.add(mnNew);

	JMenuItem mntmCard = new JMenuItem("Card");
	mntmCard.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent arg0) {
		openCardEditorDialog(new Card());
	    }
	});
	mnNew.add(mntmCard);

	JMenuItem mntmCardLayout = new JMenuItem("Card layout");
	mntmCardLayout.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent e) {
		new CardLayoutEditorDialog(self).setVisible(true);
		Main.layouts.sort(CardLayout.Comparator);
		((DefaultListModel<CardLayout>) listLayout.getModel()).clear();
		for(CardLayout layout : Main.layouts)
		    ((DefaultListModel<CardLayout>) listLayout.getModel()).addElement(layout);
	    }
	});
	mnNew.add(mntmCardLayout);

	JMenuItem mntmLoad = new JMenuItem("Load");
	mnFile.add(mntmLoad);

	JMenuItem mntmExit = new JMenuItem("Exit");
	mnFile.add(mntmExit);

	JMenu mnEdit = new JMenu("Edit");
	menuBar.add(mnEdit);

	final JMenuItem mntmSelectedCard = new JMenuItem("Selected card");
	mntmSelectedCard.setEnabled(false);
	mntmSelectedCard.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent arg0) {
		openCardEditorDialog(listCard.getSelectedValue());
	    }
	});
	mnEdit.add(mntmSelectedCard);

	final JMenuItem mntmSelectedCardLayout = new JMenuItem("Selected card layout");
	mntmSelectedCardLayout.setEnabled(false);
	mnEdit.add(mntmSelectedCardLayout);

	JMenu mnHelp = new JMenu("Help");
	menuBar.add(mnHelp);

	JMenuItem mntmAbout = new JMenuItem("About");
	mntmAbout.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent arg0) {
		JOptionPane.showMessageDialog(self, "tcgtool v"+Main.VERSION+" alpha\ntcgtool is a tool for designing TCG cards\nCopyright (C) 2019 Carlos Faruolo <5carlosfelipe5@gmail.com>", "About tcgtool", JOptionPane.INFORMATION_MESSAGE, new ImageIcon(icon));
	    }
	});
	mnHelp.add(mntmAbout);

	JSplitPane splitPane = new JSplitPane();
	splitPane.setPreferredSize(new Dimension(getSize().width/4, 0));
	splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);

	DefaultListModel<CardLayout> listLayoutModel = new DefaultListModel<CardLayout>();
	for(CardLayout layout : Main.layouts)
	    listLayoutModel.addElement(layout);
	listLayout = new JList<CardLayout>(listLayoutModel);
	listLayout.addListSelectionListener(new ListSelectionListener() {
	    public void valueChanged(ListSelectionEvent arg0) {
		mntmSelectedCardLayout.setEnabled(listLayout.getSelectedValue() != null);
	    }
	});
	listLayout.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	listLayout.setBackground(new Color(255, 255, 240));
	splitPane.setLeftComponent(listLayout);

	DefaultListModel<Card> listCardModel = new DefaultListModel<Card>();
	for(Card card : Main.cards)
	    listCardModel.addElement(card);
	listCard = new JList<Card>(listCardModel);
	listCard.addListSelectionListener(new ListSelectionListener() {
	    public void valueChanged(ListSelectionEvent arg0) {
		cardview.setCard(listCard.getSelectedValue(), (Float) spinner.getValue());
		mntmSelectedCard.setEnabled((listCard.getSelectedValue() != null));
	    }
	});
	listCard.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	splitPane.setRightComponent(listCard);
	splitPane.setDividerLocation(getSize().height/4);

	JToolBar toolBar = new JToolBar();
	toolBar.setFloatable(false);
	getContentPane().add(toolBar, BorderLayout.SOUTH);

	Component horizontalGlue = Box.createHorizontalGlue();
	toolBar.add(horizontalGlue);

	JLabel lblZoom = new JLabel("Zoom:");
	toolBar.add(lblZoom);

	spinner = new JSpinner();
	spinner.addChangeListener(new ChangeListener() {
	    public void stateChanged(ChangeEvent arg0) {
		cardview.setCard(listCard.getSelectedValue(), (Float) spinner.getValue());
	    }
	});
	spinner.setModel(new SpinnerNumberModel(100f, 1f, null, 1f));
	spinner.setMaximumSize(new Dimension(50, 100));
	spinner.setPreferredSize(new Dimension(64, 28));
	toolBar.add(spinner);

	Component horizontalStrut = Box.createHorizontalStrut(30);
	toolBar.add(horizontalStrut);

	JSplitPane splitPane_1 = new JSplitPane();
	getContentPane().add(splitPane_1, BorderLayout.CENTER);

	cardview = new CardView();
	cardview.setHorizontalAlignment(SwingConstants.CENTER);
	splitPane_1.setRightComponent(cardview);
	splitPane_1.setLeftComponent(splitPane);
    }
}
