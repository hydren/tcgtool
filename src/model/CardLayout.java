package model;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Image;
import java.awt.Point;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Comparator;
import java.util.Properties;

import control.Utils;

public class CardLayout implements Comparable<CardLayout>
{
    public static Comparator<CardLayout> Comparator = new Comparator<CardLayout>() { @Override public int compare(CardLayout l1, CardLayout l2) { return l1.compareTo(l2); }};

    public final static String LAYOUTS_FOLDER = "layouts/";

    public Properties properties = new Properties();

    public String id="", name="";

    //----------------------------------------------
    // transient stuff
    
    public Image frameImage=null, bgImage=null;
    
    //----------------------------------------------
    // card-related stuff

    public Point titlePosition = new Point();
    public Color titleFontColor = new Color(Color.BLACK.getRGB());
    public Font  titleFont=null;
    
    public Point idPosition = new Point();
    public Color idFontColor = new Color(Color.BLACK.getRGB());
    public Font  idFont=null;
    
    public Point descriptionPosition = new Point();
    public Color descriptionFontColor = new Color(Color.BLACK.getRGB());
    public Font  descriptionFont=null;
    
    public Point picturePosition = new Point();

    /** Empty constructor */
    public CardLayout(){}

    /** File constructor */
    public CardLayout(File file) throws IOException
    {
	final FileInputStream stream = new FileInputStream(file);
	try { properties.load(stream); } 
	finally { stream.close(); }

	name = properties.getProperty("name");
	picturePosition = Utils.parsePoint(properties.getProperty("picture_position"));
	titlePosition = Utils.parsePoint(properties.getProperty("title_position"));
	titleFontColor = Utils.parseColor(properties.getProperty("title_font_color"));
	idPosition = Utils.parsePoint(properties.getProperty("id_position"));
	idFontColor = Utils.parseColor(properties.getProperty("id_font_color"));
	descriptionPosition = Utils.parsePoint(properties.getProperty("description_position"));
	descriptionFontColor = Utils.parseColor(properties.getProperty("description_font_color"));
    }

    public CardLayout loadResources() throws IOException, FontFormatException
    {
	bgImage = Utils.loadImage(LAYOUTS_FOLDER+properties.getProperty("bg_image"));
	frameImage = Utils.loadImage(LAYOUTS_FOLDER+properties.getProperty("frame_image"));
	titleFont = Utils.loadFont(properties.getProperty("title_font"), properties.getProperty("title_font_size"), properties.getProperty("title_font_style"));
	idFont = Utils.loadFont(properties.getProperty("id_font"), properties.getProperty("id_font_size"), properties.getProperty("id_font_style"));
	descriptionFont = Utils.loadFont(properties.getProperty("description_font"), properties.getProperty("description_font_size"), properties.getProperty("description_font_style"));
	return this;
    }

    public String toString()
    {
	return id;
    }

    @Override
    public int compareTo(CardLayout l) {
	return id.compareTo(l.id);
    }
}
