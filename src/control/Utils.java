package control;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Image;
import java.awt.Point;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;

import javax.imageio.ImageIO;

public class Utils
{
    public static Image loadImage(String filename) throws IOException
    {
	if(filename.trim().isEmpty())
	    return null;
	else 
	    return ImageIO.read(new File(filename));
    }

    public static Font loadFont(final String name, final String sizeStr, final String styleStr) throws FontFormatException, IOException
    {
	if(name.trim().isEmpty())
	    return null;
	else
	{
	    float size = 10; 
	    int style = Font.PLAIN;

	    if(sizeStr != null && !sizeStr.trim().isEmpty())
	    {
		try {  size = Float.parseFloat(sizeStr.trim()); }
		catch (NumberFormatException e) {}
	    }

	    if(styleStr != null && !styleStr.trim().isEmpty())
	    {
		if(styleStr.trim().equals("plain"))
		    style = Font.PLAIN;
		else if(styleStr.trim().equals("bold"))
		    style = Font.BOLD;
		else if(styleStr.trim().equals("italic"))
		    style = Font.ITALIC;
		else if(styleStr.trim().equals("bold+italic"))
		    style = Font.BOLD | Font.ITALIC;
	    }

	    if(new File(name).isFile())
		return Font.createFont(Font.TRUETYPE_FONT, new File(name)).deriveFont(size).deriveFont(style);
	    else
		return new Font(name, style, (int) size);
	}
    }

    public static Point parsePoint(String pointStr)
    {
	final String[] tokens = pointStr.split(",");
	if(tokens.length != 2)
	    return new Point();
	else try { return new Point(Integer.parseInt(tokens[0].trim()), Integer.parseInt(tokens[1].trim())); }
	catch (NumberFormatException e) { return new Point(); }
    }

    public static Color parseColor(String colorStr)
    {
	final String[] tokens = colorStr.split(",");

	if(tokens.length == 1)
	{
	    for(Field f : Color.class.getFields())
		if(f.getName().equals(colorStr.trim()))
		    try { return (Color) f.get(null); } 
	    catch (IllegalArgumentException e) {} 
	    catch (IllegalAccessException e) {}
	    return Color.black;
	}
	else if(tokens.length != 3 && tokens.length != 4)
	    return Color.black;
	else try 
	{ 
	    if(tokens.length == 3)
		return new Color(Integer.parseInt(tokens[0].trim()), Integer.parseInt(tokens[1].trim()), Integer.parseInt(tokens[2].trim()));
	    else
		return new Color(Integer.parseInt(tokens[0].trim()), Integer.parseInt(tokens[1].trim()), Integer.parseInt(tokens[2].trim()), Integer.parseInt(tokens[3].trim()));
	}
	catch (NumberFormatException e) { return Color.black; }
    }
}
