package control;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.Vector;

import javax.swing.UIManager;

import model.Card;
import model.CardLayout;
import view.MainWindow;

public class Main 
{
    public static final String VERSION = "0.2";

    public static final Vector<CardLayout> layouts = new Vector<CardLayout>();
    public static final Vector<Card> cards = new Vector<Card>();

    public static final FileFilter propertiesFileFilter = new FileFilter() { @Override public boolean accept(File f) { return f.isFile() && f.getName().endsWith(".properties"); }};

    public static CardLayout findLayout(String name)
    {
	for(CardLayout layout : layouts) 
	    if(layout.id.equals(name))
		return layout;

	throw new NoSuchElementException();
    }

    public static void main(String[] args) 
    {
	try { UIManager.setLookAndFeel(System.getProperty("os.name", "").equalsIgnoreCase("linux")? "com.sun.java.swing.plaf.gtk.GTKLookAndFeel" : UIManager.getSystemLookAndFeelClassName()); } 
	catch (Exception e) { System.err.println("Failed to change look and feel: "+e.getLocalizedMessage()); }

	for(File file : new File(CardLayout.LAYOUTS_FOLDER).listFiles(propertiesFileFilter))
	{
	    CardLayout layout;
	    try { layout = new CardLayout(file); } 
	    catch (IOException e) 
	    {
		System.err.println("Error while loading card layout "+file.getName()+": "+e.getLocalizedMessage());
		continue;
	    }
	    layout.id = file.getName().replaceFirst("[.][^.]+$", "");;
	    layouts.add(layout);
	}

	for(File file : new File(Card.CARDS_FOLDER).listFiles(propertiesFileFilter))
	{
	    Card card;
	    try { card = new Card(file); } 
	    catch (IOException e) 
	    {
		System.err.println("Error while loading card "+file.getName()+": "+e.getLocalizedMessage());
		continue;
	    }
	    card.layout = findLayout(card.properties.getProperty("layout"));
	    cards.add(card);
	}

	for(CardLayout layout : layouts)
	    try { layout.loadResources(); } 
	catch (Exception e) 
	{
	    System.err.println("Error while loading card layout resources for "+layout.name+" (id "+layout.id+"):"+e.getLocalizedMessage());
	    continue;
	}

	for(Card card : cards)
	    try { card.loadResources(); } 
	catch (Exception e) 
	{
	    System.err.println("Error while loading card resources for "+card.title+" (id "+card.id+"):"+e.getLocalizedMessage());
	    continue;
	}

	Main.layouts.sort(CardLayout.Comparator);
	Main.cards.sort(Card.Comparator);
	new MainWindow().setVisible(true);
    }
}
